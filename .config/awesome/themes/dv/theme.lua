theme = {}

theme.dir  = os.getenv("HOME") .. "/.config/awesome/themes/dv"
theme.font = 'JetBrains Mono medium 8'
theme.bg_normal             = "#252525"
theme.border_width          = 20
theme.wallpaper             = theme.dir .. "/arch.png"
theme.archIcon              = theme.dir .. "/Arch-linux-logo.png"
theme.border_normal         = "#252525"
theme.border_width_normal   = 0
theme.border_focus          = "#f5a119"
theme.border_width_focus    = 2
theme.taglist_bg_focus      = "#f5a119"
theme.taglist_fg_focus      = "#ffffff"
theme.tasklist_bg_focus     = "#f5a119"
theme.tasklist_bg_normal    = "#0e507a"
theme.taglist_fg_occupied   = "#f5a119"

function theme.at_screen_connect(s)

end

return theme