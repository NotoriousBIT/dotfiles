#!/bin/sh
sudo locale-gen

# Install git
mkdir -p ~/git
sudo pacman -S git openssh --noconfirm

# Setup env
cd ~/git && git clone http://gitlab.com/NotoriousBIT/dotfiles.git
ln -sf ~/git/dotfiles/.config ~/
ln -sf ~/git/dotfiles/.xinitrc ~/
ln -sf ~/git/dotfiles/.zshrc ~/
ln -sf ~/git/dotfiles/.zprofile ~/
ln -sf ~/git/dotfiles/.p10k.zsh ~/

cd ~/git/dotfiles && git remote set-url origin git@gitlab.com:NotoriousBIT/dotfiles.git

# Install paru
cd ~/git && git clone https://aur.archlinux.org/paru.git
cd ~/git/paru && makepkg -si

# Install alacitty
paru -Sy alacritty zsh --noconfirm
chsh -s $(which zsh) $(whoami)

# oh-my-zsh
paru -Sy zsh-completions oh-my-zsh-git zsh-theme-powerlevel10k-git --noconfirm
# zsh-syntax-highlighting
paru -Sy zsh-syntax-highlighting --noconfirm
sudo ln -sf /usr/share/zsh/plugins/zsh-syntax-highlighting /usr/share/oh-my-zsh/custom/plugins/
# zsh-autosuggestions
paru -Sy zsh-autosuggestions-git --noconfirm
sudo ln -sf /usr/share/zsh/plugins/zsh-autosuggestions /usr/share/oh-my-zsh/custom/plugins/

# Install xorg
paru -Sy xorg xorg-drivers xorg-xinit --noconfirm
sudo localectl --no-convert set-keymap de-latin1-nodeadkeys
sudo localectl --no-convert set-x11-keymap de pc105 deadgraveacute

# Install audio
paru -Sy pulseaudio pulseaudio-alsa pavucontrol --noconfirm

# Install dmenu
paru -Sy dmenu --noconfirm

# Install alacritty
paru -Sy alacritty --noconfirm

# Install awesome wm
paru -Sy awesome --noconfirm

# Install picom
paru -Sy picom --noconfirm

# Install Styling stuff
paru -Sy ttf-jetbrains-mono powerline-fonts nerd-fonts-hack adobe-source-code-pro-fonts ttf-nerd-fonts-symbols ttf-font-awesome lain-git --noconfirm

# Install browsers
paru -Sy firefox chromium --noconfirm

# Install VsCode
paru -Sy vscode gnome-keyring libsecret libgnome-keyring --noconfirm

# Install nvm (Node Version Manager)
paru -Sy nvm --noconfirm
ln -sf ~/git/dotfiles/.nvmrc ~/
nvm install

# AWS-CDK
paru -Sy aws-cdk aws-cli --noconfirm

# Docker
paru -Sy docker docker-compose --noconfirm
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker $(whoami)

# Utils
paru -Sy nmap htop unzip --noconfirm

# Openvpn
paru -Sy openvpn openvpn-update-resolv-conf-git --noconfirm